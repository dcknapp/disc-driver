package net.cyberratt.helper

import net.cyberratt.helper.ScoreHelper.GameHalf.FRONT
import net.cyberratt.helper.ScoreHelper.toScoreList
import java.util.HashMap
import java.util.stream.Collectors

/**
 * Helper class providing functions for converting and calculating various score collections.
 */
object ScoreHelper {

  /**
   * Converts a hole-number-keyed map to a delimited score string.
   *
   * @param scores hole-numbered map of scores
   * @return '|'-delimited string of hole scores
   */
  fun toDelimitedString(scores: Map<Int, Int>): String {
    val scoreString = StringBuilder()
    for (i in 1..scores.size) {
      val score = scores[i]
      scoreString.append(score)
      if (i < scores.size) {
        scoreString.append("|")
      }
    }

    return scoreString.toString()
  }

  /**
   * Converts a delimited score string to a map of scores.
   * The key is the hole number, the value is the score.
   *
   * @param delimitedString '|'-separated string of hole scores
   * @return map of hole numbers to scores
   */
  fun toHoleNumberedMap(delimitedString: String): Map<Int, Int> {
    return delimitedString
      .split("|")
      .mapIndexed { index, s -> (index + 1) to s.toInt() }
      .toMap()
  }

  /**
   * Converts a delimited score string to a list of integer score values
   *
   * @param delimitedString '|'-separated string of hole scores
   * @return
   */
  fun toScoreList(delimitedString: String): List<Int> {
    return toHoleNumberedMap(delimitedString).values.toList()
  }

  /**
   * Retrieves the score for the second half holes from a delimited score string.
   *
   * @param delimitedString  '|'-separated string of hole scores
   * @return
   */
  fun getBackScore(delimitedString: String): Int {
    return getHalfScoreFromScoreList(toScoreList(delimitedString), GameHalf.BACK)
  }

  /**
   * Retrieves the score for the first half holes from a delimited score string.
   *
   * @param delimitedString  '|'-separated string of hole scores
   * @return
   */
  fun getFrontScore(delimitedString: String): Int {
    return getHalfScoreFromScoreList(toScoreList(delimitedString), GameHalf.FRONT)
  }

  /**
   * Collects scores given a specified desired half of a game.
   *
   * If courseEntity has even holes, should split the holes evenly.
   * If odd holes, front score should contain the extra hole's score.
   *
   * @param scores list of hole scores
   * @param half enum value representing front or back set of holes for a courseEntity
   * @return
   */
  private fun getHalfScoreFromScoreList(
    scores: List<Int>,
    half: GameHalf
  ): Int {
    if (scores.size <= 9) return scores.sum()

    val (start: Int, end: Int) = getSublistIndices(half, scores)

    return scores.subList(start, end).sum()
  }

  private fun getSublistIndices(
    half: GameHalf,
    scores: List<Int>
  ): Pair<Int, Int> {
    return if (half == FRONT) {
      Pair(0, getFirstBackHalfHoleNumber(scores.size))
    } else {
      Pair(getFirstBackHalfHoleNumber(scores.size), scores.size)
    }
  }

  /**
   * Calculates the halfway point of a set of holes to divide the front and back scores.
   *
   * @param numHoles number of holes
   * @return
   */
  fun getFirstBackHalfHoleNumber(numHoles: Int): Int {
    return if (numHoles % 2 == 0)
      numHoles / 2
    else
      numHoles / 2 + 1
  }

  /**
   * Calculates the total of a given delimited score string.
   *
   * @param delimitedString '|'-separated string of hole scores
   * @return
   */
  fun getTotalScore(delimitedString: String): Int {
    return delimitedString
      .split("|")
      .sumBy { it.toInt() }
  }

  /**
   * Internal enum used as a classification for divisions of a set of holes.
   */
  private enum class GameHalf {
    FRONT, BACK
  }
}
