package net.cyberratt.service

import net.cyberratt.web.model.Note
import net.cyberratt.database.entity.converters.NoteEntityConverter
import net.cyberratt.database.entity.updater.NoteEntityUpdater
import net.cyberratt.database.repository.NoteRepository
import net.cyberratt.exception.EntityNotFoundException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.UUID

@Service
open class NoteService @Autowired constructor(
  private val noteRepository: NoteRepository,
  private val converter: NoteEntityConverter,
  private val updater: NoteEntityUpdater)
  : EntityService<Note> {

  override fun getAllEntities(): List<Note> = noteRepository.findAll().map { converter.toApiModel(it) }

  override fun getEntityById(id: String): Note {
    val entity = noteRepository.findOne(UUID.fromString(id)) ?: throw EntityNotFoundException()
    return converter.toApiModel(entity)
  }

  @Transactional
  override fun deleteEntity(id: String): Boolean {

    val toDelete = noteRepository.findOne(UUID.fromString(id)) ?: return false

    noteRepository.delete(toDelete)

    return true
  }

  @Transactional
  override fun updateEntity(apiModel: Note): Note {

    val toUpdate = noteRepository.findOne(UUID.fromString(apiModel.id)) ?: throw EntityNotFoundException()
    val updated = updater.getUpdatedEntity(toUpdate, apiModel)

    return converter.toApiModel(noteRepository.save(updated))
  }

  @Transactional
  override fun createEntity(apiModel: Note): Note {
    return converter.toApiModel(noteRepository.save(converter.toEntity(apiModel)))
  }
}
