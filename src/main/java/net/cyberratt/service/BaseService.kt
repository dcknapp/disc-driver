package net.cyberratt.service

import net.cyberratt.database.entity.updater.EntityUpdater
import net.cyberratt.web.model.ApiModel
import net.cyberratt.database.entity.converters.EntityConverter
import net.cyberratt.database.entity.BaseEntity
import net.cyberratt.exception.EntityNotFoundException
import net.cyberratt.exception.EntityProviderException
import net.cyberratt.exception.ExceptionMessage.UNKNOWN_ERROR
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.transaction.annotation.Transactional
import java.util.UUID

abstract class BaseService<A : ApiModel, E : BaseEntity> : EntityService<A> {

  abstract val repository: PagingAndSortingRepository<E, UUID>
  abstract val converter: EntityConverter<A, E>
  abstract val entityUpdater: EntityUpdater<E, A>

  override fun getAllEntities(): List<A> = repository.findAll().map { converter.toApiModel(it) }

  override fun getEntityById(id: String): A {
    val entity = repository.findOne(UUID.fromString(id)) ?: throw EntityNotFoundException()
    return converter.toApiModel(entity)
  }

  @Transactional
  override fun deleteEntity(id: String): Boolean {
    val toDelete = repository.findOne(UUID.fromString(id)) ?: return false

    repository.delete(toDelete)

    return true
  }

  @Transactional
  override fun updateEntity(apiModel: A): A {
    val toUpdate = repository.findOne(UUID.fromString(apiModel.id)) ?: throw EntityNotFoundException()
    val updated = entityUpdater.getUpdatedEntity(toUpdate, apiModel)

    return converter.toApiModel(repository.save(updated))
  }

  @Transactional
  override fun createEntity(apiModel: A): A {
    val toSave = converter.toEntity(apiModel)
    val saved = repository.save(toSave) ?: throw EntityProviderException(UNKNOWN_ERROR)

    return converter.toApiModel(saved)
  }
}
