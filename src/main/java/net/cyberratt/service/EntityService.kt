package net.cyberratt.service

/**
 * EntityService class to perform duties required by entity controllers.
 *
 * @param <T>
</T> */
interface EntityService<T> {

  /**
   * Retrieves all entities.
   *
   * @return
   */
  fun getAllEntities(): List<T>

  /**
   * Retrieves specific entity by ID.
   *
   * @param id
   * @return
   */
  fun getEntityById(id: String): T

  /**
   * Deletes specific entity by ID.
   *
   * @param id
   * @return
   */
  fun deleteEntity(id: String): Boolean

  /**
   * Updates previously persisted entity.
   *
   * @param entity
   * @return
   */
  fun updateEntity(apiModel: T): T

  /**
   * Creates and persists a new entity.
   *
   * @param entity
   * @return
   */
  fun createEntity(apiModel: T): T

}
