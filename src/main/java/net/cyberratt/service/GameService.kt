package net.cyberratt.service

import net.cyberratt.web.model.Game
import net.cyberratt.database.entity.converters.GameEntityConverter
import net.cyberratt.database.entity.updater.GameEntityUpdater
import net.cyberratt.database.repository.GameRepository
import net.cyberratt.exception.EntityNotFoundException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.UUID

@Service
open class GameService @Autowired constructor(
  private val gameRepository: GameRepository,
  private val converter: GameEntityConverter,
  private val updater: GameEntityUpdater)
  : EntityService<Game> {

  override fun getAllEntities(): List<Game> = gameRepository.findAll().map { converter.toApiModel(it) }

  override fun getEntityById(id: String): Game {
    val entity = gameRepository.findOne(UUID.fromString(id)) ?: throw EntityNotFoundException()
    return converter.toApiModel(entity)
  }

  @Transactional
  override fun deleteEntity(id: String): Boolean {

    val toDelete = gameRepository.findOne(UUID.fromString(id)) ?: throw EntityNotFoundException()

    gameRepository.delete(toDelete)

    return true
  }

  @Transactional
  override fun updateEntity(apiModel: Game): Game {

    val toUpdate = gameRepository.findOne(UUID.fromString(apiModel.id)) ?: throw EntityNotFoundException()
    val updated = updater.getUpdatedEntity(toUpdate, apiModel)

    return converter.toApiModel(gameRepository.save(updated))
  }

  @Transactional
  override fun createEntity(apiModel: Game): Game = converter.toApiModel(gameRepository.save(converter.toEntity(apiModel)))
}
