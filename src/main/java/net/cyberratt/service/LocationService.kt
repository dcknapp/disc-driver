package net.cyberratt.service

import net.cyberratt.web.model.Location
import net.cyberratt.database.entity.converters.LocationEntityConverter
import net.cyberratt.database.entity.updater.LocationEntityUpdater
import net.cyberratt.database.repository.LocationRepository
import net.cyberratt.exception.EntityNotFoundException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.UUID

@Service
open class LocationService @Autowired constructor(
  private val locationRepository: LocationRepository,
  private val converter: LocationEntityConverter,
  private val updater: LocationEntityUpdater
) : EntityService<Location> {

  override fun getAllEntities(): List<Location> = locationRepository.findAll().map { converter.toApiModel(it) }

  override fun getEntityById(id: String): Location {
    val entity = locationRepository.findOne(UUID.fromString(id)) ?: throw EntityNotFoundException()
    return converter.toApiModel(entity)
  }

  @Transactional
  override fun deleteEntity(id: String): Boolean {
    val toDelete = locationRepository.findOne(UUID.fromString(id)) ?: return false

    locationRepository.delete(toDelete)

    return true
  }

  @Transactional
  override fun updateEntity(apiModel: Location): Location {
    val toUpdate = locationRepository.findOne(UUID.fromString(apiModel.id)) ?: throw EntityNotFoundException()
    val updated = updater.getUpdatedEntity(toUpdate, apiModel)

    return converter.toApiModel(locationRepository.save(updated))
  }

  @Transactional
  override fun createEntity(apiModel: Location): Location {
    return converter.toApiModel(locationRepository.save(converter.toEntity(apiModel)))
  }
}
