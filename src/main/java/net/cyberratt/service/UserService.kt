package net.cyberratt.service

import net.cyberratt.web.model.User
import net.cyberratt.database.entity.converters.UserEntityConverter
import net.cyberratt.database.entity.updater.UserEntityUpdater
import net.cyberratt.database.repository.UserRepository
import net.cyberratt.exception.AuthenticationException
import net.cyberratt.exception.EntityNotFoundException
import net.cyberratt.web.requests.FriendRequest
import net.cyberratt.web.requests.LoginRequest
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.UUID

@Service
open class UserService @Autowired constructor(
  private val repository: UserRepository,
  private val converter: UserEntityConverter,
  private val updater: UserEntityUpdater,
  private val passwordEncoder: BCryptPasswordEncoder
) : EntityService<User> {

  override fun getAllEntities(): List<User> = repository.findAll().map { converter.toApiModel(it) }

  override fun getEntityById(id: String): User {
    val entity = repository.findOne(UUID.fromString(id)) ?: throw EntityNotFoundException()
    return converter.convertWithFriends(entity)
  }

  @Transactional
  override fun deleteEntity(id: String): Boolean {

    val toDelete = repository.findOne(UUID.fromString(id)) ?: return false

    repository.delete(toDelete)

    return true
  }

  @Transactional
  override fun updateEntity(apiModel: User): User {
    val toUpdate = repository.findOne(UUID.fromString(apiModel.id)) ?: throw EntityNotFoundException()
    val updated = updater.getUpdatedEntity(toUpdate, apiModel)

    return converter.toApiModel(repository.save(updated))
  }

  @Transactional
  override fun createEntity(apiModel: User): User {
    apiModel.passwordHash = passwordEncoder.encode(apiModel.password)

    val user = repository.save(converter.toEntity(apiModel))

    logger.warn("Saved entity: $user")
    return converter.toApiModel(user)
  }

  open fun login(loginRequest: LoginRequest): User {
    logger.info("logging in with: $loginRequest")

    val user = repository.findByEmail(loginRequest.email) ?: throw AuthenticationException()

    if (!passwordEncoder.matches(loginRequest.password, user.passwordHash)) {
      logger.error("password hashes don't match")
      throw AuthenticationException()
    }

    logger.info("Success: $user")
    return converter.toApiModel(user)
  }

  open fun addFriend(request: FriendRequest): Boolean {
    logger.info("adding friend with: $request")

    val user = repository.findOne(UUID.fromString(request.userId)) ?: throw EntityNotFoundException()
    val friend = repository.findOne(UUID.fromString(request.friendId)) ?: throw EntityNotFoundException()

    return if (user.friends.contains(friend)) {
      false
    } else {
      repository.save(user.apply { friends.add(friend) }) != null
    }
  }

  open fun getFriends(id: String): List<User> {
    val user = repository.findOne(UUID.fromString(id)) ?: throw EntityNotFoundException()
    return converter.convertWithFriends(user).friends
  }

  companion object {
    private val logger = LoggerFactory.getLogger(UserService::class.java)
  }
}
