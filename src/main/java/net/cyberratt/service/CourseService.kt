package net.cyberratt.service

import net.cyberratt.web.model.Course
import net.cyberratt.database.entity.converters.CourseEntityConverter
import net.cyberratt.database.entity.updater.CourseEntityUpdater
import net.cyberratt.database.repository.CourseRepository
import net.cyberratt.exception.EntityNotFoundException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.UUID

@Service
open class CourseService @Autowired constructor(
  private val courseRepository: CourseRepository,
  private val converter: CourseEntityConverter,
  private val updater: CourseEntityUpdater)
  : EntityService<Course> {

  override fun getAllEntities(): List<Course> = courseRepository.findAll().map { converter.toApiModel(it) }

  override fun getEntityById(id: String): Course {
    val entity = courseRepository.findOne(UUID.fromString(id)) ?: throw EntityNotFoundException()

    return converter.toApiModel(entity)
  }

  @Transactional
  override fun deleteEntity(id: String): Boolean {
    val toDelete = courseRepository.findOne(UUID.fromString(id)) ?: return false

    courseRepository.delete(toDelete)

    return true
  }

  @Transactional
  override fun updateEntity(apiModel: Course): Course {
    val toUpdate = courseRepository.findOne(UUID.fromString(apiModel.id)) ?: throw EntityNotFoundException()
    val updated = updater.getUpdatedEntity(toUpdate, apiModel)

    return converter.toApiModel(courseRepository.save(updated))
  }

  @Transactional
  override fun createEntity(apiModel: Course): Course {
    return converter.toApiModel(courseRepository.save(converter.toEntity(apiModel)))
  }
}
