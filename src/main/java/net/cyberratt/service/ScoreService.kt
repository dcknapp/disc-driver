package net.cyberratt.service

import net.cyberratt.web.model.Score
import net.cyberratt.database.entity.converters.ScoreEntityConverter
import net.cyberratt.database.entity.updater.ScoreEntityUpdater
import net.cyberratt.database.repository.ScoreRepository
import net.cyberratt.exception.EntityNotFoundException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.UUID

@Service
open class ScoreService @Autowired constructor(
  private val scoreRepository: ScoreRepository,
  private val converter: ScoreEntityConverter,
  private val updater: ScoreEntityUpdater)
  : EntityService<Score> {

  override fun getAllEntities(): List<Score> = scoreRepository.findAll().map { converter.toApiModel(it) }

  override fun getEntityById(id: String): Score {
    val entity = scoreRepository.findOne(UUID.fromString(id)) ?: throw EntityNotFoundException()
    return converter.toApiModel(entity)
  }

  @Transactional
  override fun deleteEntity(id: String): Boolean {
    val toDelete = scoreRepository.findOne(UUID.fromString(id)) ?: return false

    scoreRepository.delete(toDelete)

    return true
  }

  @Transactional
  override fun updateEntity(apiModel: Score): Score {

    val toUpdate = scoreRepository.findOne(UUID.fromString(apiModel.id)) ?: throw EntityNotFoundException()
    val updated = updater.getUpdatedEntity(toUpdate, apiModel)

    return converter.toApiModel(scoreRepository.save(updated))
  }

  @Transactional
  override fun createEntity(apiModel: Score): Score {
    return converter.toApiModel(scoreRepository.save(converter.toEntity(apiModel)))
  }
}
