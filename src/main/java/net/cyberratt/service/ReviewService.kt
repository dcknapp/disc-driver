package net.cyberratt.service

import net.cyberratt.web.model.Review
import net.cyberratt.database.entity.converters.ReviewEntityConverter
import net.cyberratt.database.entity.updater.ReviewEntityUpdater
import net.cyberratt.database.repository.ReviewRepository
import net.cyberratt.exception.EntityNotFoundException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.UUID

@Service
open class ReviewService @Autowired constructor(
  private val reviewRepository: ReviewRepository,
  private val converter: ReviewEntityConverter,
  private val updater: ReviewEntityUpdater)
  : EntityService<Review> {

  override fun getAllEntities(): List<Review> = reviewRepository.findAll().map { converter.toApiModel(it) }

  override fun getEntityById(id: String): Review {
    val entity = reviewRepository.findOne(UUID.fromString(id)) ?: throw EntityNotFoundException()
    return converter.toApiModel(entity)
  }

  @Transactional
  override fun deleteEntity(id: String): Boolean {
    val toDelete = reviewRepository.findOne(UUID.fromString(id)) ?: return false

    reviewRepository.delete(toDelete)

    return true
  }

  @Transactional
  override fun updateEntity(apiModel: Review): Review {

    val toUpdate = reviewRepository.findOne(UUID.fromString(apiModel.id)) ?: throw EntityNotFoundException()
    val updated = updater.getUpdatedEntity(toUpdate, apiModel)

    return converter.toApiModel(reviewRepository.save(updated))
  }

  @Transactional
  override fun createEntity(apiModel: Review): Review {
    return converter.toApiModel(reviewRepository.save(converter.toEntity(apiModel)))
  }
}
