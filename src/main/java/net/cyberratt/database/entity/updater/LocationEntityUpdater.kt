package net.cyberratt.database.entity.updater

import net.cyberratt.web.model.Location
import net.cyberratt.database.entity.converters.EntityConverter
import net.cyberratt.database.entity.LocationEntity
import org.springframework.stereotype.Component

@Component
class LocationEntityUpdater(private val entityConverter: EntityConverter<Location, LocationEntity>)
  : EntityUpdater<LocationEntity, Location> {

  override fun getUpdatedEntity(entity: LocationEntity, apiModel: Location): LocationEntity {
    return LocationEntity()
  }
}
