package net.cyberratt.database.entity.updater

import net.cyberratt.web.model.ApiModel
import net.cyberratt.database.entity.BaseEntity

interface EntityUpdater<E : BaseEntity, A : ApiModel> {
  fun getUpdatedEntity(entity: E, apiModel: A): E
}
