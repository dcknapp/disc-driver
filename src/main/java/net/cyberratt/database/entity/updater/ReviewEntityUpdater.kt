package net.cyberratt.database.entity.updater

import net.cyberratt.web.model.Review
import net.cyberratt.database.entity.converters.EntityConverter
import net.cyberratt.database.entity.ReviewEntity
import org.springframework.stereotype.Component

@Component
class ReviewEntityUpdater(private val entityConverter: EntityConverter<Review, ReviewEntity>)
  : EntityUpdater<ReviewEntity, Review> {

  override fun getUpdatedEntity(entity: ReviewEntity, apiModel: Review): ReviewEntity {
    return ReviewEntity()
  }
}
