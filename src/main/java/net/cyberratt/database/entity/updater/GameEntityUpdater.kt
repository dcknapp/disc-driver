package net.cyberratt.database.entity.updater

import net.cyberratt.web.model.Game
import net.cyberratt.database.entity.converters.EntityConverter
import net.cyberratt.database.entity.GameEntity
import org.springframework.stereotype.Component

@Component
class GameEntityUpdater(private val entityConverter: EntityConverter<Game, GameEntity>)
  : EntityUpdater<GameEntity, Game> {

  override fun getUpdatedEntity(entity: GameEntity, apiModel: Game): GameEntity {
    return GameEntity()
  }
}
