package net.cyberratt.database.entity.updater

import net.cyberratt.web.model.Score
import net.cyberratt.database.entity.converters.EntityConverter
import net.cyberratt.database.entity.ScoreEntity
import org.springframework.stereotype.Component

@Component
class ScoreEntityUpdater(private val entityConverter: EntityConverter<Score, ScoreEntity>)
  : EntityUpdater<ScoreEntity, Score> {

  override fun getUpdatedEntity(entity: ScoreEntity, apiModel: Score): ScoreEntity {
    return ScoreEntity()
  }
}
