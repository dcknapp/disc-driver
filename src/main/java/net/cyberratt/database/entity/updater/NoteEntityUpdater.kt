package net.cyberratt.database.entity.updater

import net.cyberratt.web.model.Note
import net.cyberratt.database.entity.converters.EntityConverter
import net.cyberratt.database.entity.NoteEntity
import org.springframework.stereotype.Component

@Component
class NoteEntityUpdater(private val entityConverter: EntityConverter<Note, NoteEntity>)
  : EntityUpdater<NoteEntity, Note> {

  override fun getUpdatedEntity(entity: NoteEntity, apiModel: Note): NoteEntity {
    return NoteEntity()
  }
}
