package net.cyberratt.database.entity.updater

import net.cyberratt.web.model.Course
import net.cyberratt.database.entity.converters.EntityConverter
import net.cyberratt.database.entity.CourseEntity
import org.springframework.stereotype.Component

@Component
class CourseEntityUpdater(private val entityConverter: EntityConverter<Course, CourseEntity>)
  : EntityUpdater<CourseEntity, Course> {

  override fun getUpdatedEntity(entity: CourseEntity, apiModel: Course): CourseEntity {
    return CourseEntity()
  }
}
