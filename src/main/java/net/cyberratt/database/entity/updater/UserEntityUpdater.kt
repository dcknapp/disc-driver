package net.cyberratt.database.entity.updater

import net.cyberratt.web.model.User
import net.cyberratt.database.entity.converters.EntityConverter
import net.cyberratt.database.entity.UserEntity
import org.springframework.stereotype.Component

@Component
class UserEntityUpdater(private val entityConverter: EntityConverter<User, UserEntity>)
  : EntityUpdater<UserEntity, User> {

  override fun getUpdatedEntity(entity: UserEntity, apiModel: User): UserEntity {
    with (apiModel) {
      if (firstName == null) firstName = entity.firstName
      if (lastName == null) lastName = entity.lastName
      if (email == null) email = entity.email
      if (nickname == null) nickname = entity.nickname
    }

    return entityConverter.toEntity(apiModel).apply {
      id = entity.id
      passwordHash = entity.passwordHash
      created = entity.created
      location = entity.location
    }
  }
}
