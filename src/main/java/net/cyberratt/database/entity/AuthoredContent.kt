package net.cyberratt.database.entity

import javax.persistence.*

@MappedSuperclass
abstract class AuthoredContent(

  @ManyToOne
  @JoinColumn(name = "author_id")
  var author: UserEntity? = null,

  @Column(name = "content")
  var content: String? = null

) : BaseEntity()
