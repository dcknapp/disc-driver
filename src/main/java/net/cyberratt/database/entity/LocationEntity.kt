package net.cyberratt.database.entity

import java.util.UUID
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "locations")
class LocationEntity(

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "location_id")
  override var id: UUID? = null,

  @Column(name = "latitude")
  var latitude: Double = 0.0,

  @Column(name = "longitude")
  var longitude: Double = 0.0,

  @Column(name = "street_address")
  var streetAddress: String? = null,

  @Column(name = "city")
  var city: String? = null,

  @Column(name = "state")
  var state: String? = null,

  @Column(name = "country")
  var country: String? = null,

  @Column(name = "zip_code")
  var zipCode: Int? = null

) : BaseEntity() {

  override fun toString(): String {
    return "LocationEntity(id=$id, latitude=$latitude, longitude=$longitude, streetAddress=$streetAddress, city=$city, state=$state, country=$country, zipCode=$zipCode)"
  }
}
