package net.cyberratt.database.entity

import net.cyberratt.helper.ScoreHelper
import java.util.UUID
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table

@Entity
@Table(name = "scores")
class ScoreEntity(

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "score_id")
  override var id: UUID? = null,

  @ManyToOne
  @JoinColumn(name = "user_id")
  var userEntity: UserEntity? = null,

  @Column(name = "hole_scores")
  var holeScores: String = ""

) : BaseEntity() {

  fun scoreMap(): Map<Int, Int> {
    return ScoreHelper.toHoleNumberedMap(holeScores)
  }

  fun setScores(scores: Map<Int, Int>) {
    this.holeScores = ScoreHelper.toDelimitedString(scores)
  }

  override fun toString(): String {
    return "ScoreEntity(id=$id, userEntity=$userEntity, holeScores='$holeScores')"
  }
}
