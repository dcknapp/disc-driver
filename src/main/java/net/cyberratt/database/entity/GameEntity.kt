package net.cyberratt.database.entity

import java.util.Date
import java.util.UUID
import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.JoinTable
import javax.persistence.ManyToOne
import javax.persistence.OneToMany
import javax.persistence.Table
import javax.persistence.Temporal
import javax.persistence.TemporalType

@Entity
@Table(name = "games")
class GameEntity(

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "game_id")
  override var id: UUID? = null,

  @ManyToOne
  @JoinColumn(name = "course_id")
  var courseEntity: CourseEntity? = null,

  @Column(name = "game_start")
  @Temporal(TemporalType.TIMESTAMP)
  var gameStart: Date? = null,

  @Column(name = "game_end")
  @Temporal(TemporalType.TIMESTAMP)
  var gameEnd: Date? = null,

  @OneToMany(cascade = [CascadeType.PERSIST])
  @JoinTable(
    name = "games_scores",
    joinColumns = [JoinColumn(name = "game_id")],
    inverseJoinColumns = [JoinColumn(name = "score_id")]
  )
  var scores: List<ScoreEntity>? = null

) : BaseEntity() {

  override fun toString(): String {
    return "GameEntity(id=$id, courseEntity=$courseEntity, gameStart=$gameStart, gameEnd=$gameEnd, scores=$scores)"
  }
}
