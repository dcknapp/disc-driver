package net.cyberratt.database.entity.converters

import net.cyberratt.web.model.Note
import net.cyberratt.database.entity.NoteEntity
import org.springframework.stereotype.Component

@Component
class NoteEntityConverter : EntityConverter<Note, NoteEntity> {

  override fun toApiModel(entity: NoteEntity) = Note()

  override fun toEntity(apiModel: Note) = NoteEntity()
}
