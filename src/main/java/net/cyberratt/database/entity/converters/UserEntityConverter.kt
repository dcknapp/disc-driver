package net.cyberratt.database.entity.converters

import net.cyberratt.web.model.User
import net.cyberratt.database.entity.UserEntity
import org.springframework.stereotype.Component
import java.util.UUID

@Component
class UserEntityConverter(private val locationEntityConverter: LocationEntityConverter) : EntityConverter<User, UserEntity> {

  fun convertWithFriends(entity: UserEntity): User {
    return convertToUser(entity).apply {
      friends = entity.friends.map { toApiModel(it) }.toList()
    }
  }

  override fun toApiModel(entity: UserEntity): User {
    return convertToUser(entity)
  }

  private fun convertToUser(entity: UserEntity): User {
    return User(
      id = entity.id?.toString(),
      firstName = entity.firstName,
      lastName = entity.lastName,
      email = entity.email,
      nickname = entity.nickname,
      isEnabled = entity.isEnabled,
      location = entity.location?.let { locationEntityConverter.toApiModel(it) },
      created = entity.created,
      modified = entity.modified
    )
  }

  override fun toEntity(apiModel: User): UserEntity {
    return UserEntity(
      id = apiModel.id?.let { UUID.fromString(it) },
      firstName = apiModel.firstName,
      lastName = apiModel.lastName,
      email = apiModel.email,
      passwordHash = apiModel.passwordHash,
      nickname = apiModel.nickname,
      isEnabled = apiModel.isEnabled,
      location = apiModel.location?.let { locationEntityConverter.toEntity(it) }
    ).also {
      if (apiModel.created != null) it.created = apiModel.created!!
      if (apiModel.modified != null) it.modified = apiModel.modified!!
    }
  }
}

