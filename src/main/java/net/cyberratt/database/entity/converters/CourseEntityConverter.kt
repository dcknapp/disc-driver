package net.cyberratt.database.entity.converters

import net.cyberratt.web.model.Course
import net.cyberratt.database.entity.CourseEntity
import org.springframework.stereotype.Component

@Component
class CourseEntityConverter : EntityConverter<Course, CourseEntity> {

  override fun toApiModel(entity: CourseEntity) = Course()

  override fun toEntity(apiModel: Course) = CourseEntity()
}
