package net.cyberratt.database.entity.converters

import net.cyberratt.web.model.Game
import net.cyberratt.database.entity.GameEntity
import org.springframework.stereotype.Component

@Component
class GameEntityConverter : EntityConverter<Game, GameEntity> {

  override fun toApiModel(entity: GameEntity) = Game()

  override fun toEntity(apiModel: Game) = GameEntity()
}
