package net.cyberratt.database.entity.converters

import net.cyberratt.web.model.Review
import net.cyberratt.database.entity.ReviewEntity
import org.springframework.stereotype.Component

@Component
class ReviewEntityConverter : EntityConverter<Review, ReviewEntity> {

  override fun toApiModel(entity: ReviewEntity) = Review()

  override fun toEntity(apiModel: Review) = ReviewEntity()
}
