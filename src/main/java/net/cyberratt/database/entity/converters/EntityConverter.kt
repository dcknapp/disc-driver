package net.cyberratt.database.entity.converters

import net.cyberratt.database.entity.BaseEntity

interface EntityConverter<A, E : BaseEntity> {

  fun toApiModel(entity: E): A

  fun toEntity(apiModel: A): E
}
