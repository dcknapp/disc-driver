package net.cyberratt.database.entity.converters

import net.cyberratt.web.model.Score
import net.cyberratt.database.entity.ScoreEntity
import org.springframework.stereotype.Component

@Component
class ScoreEntityConverter : EntityConverter<Score, ScoreEntity> {

  override fun toApiModel(entity: ScoreEntity) = Score()

  override fun toEntity(apiModel: Score) = ScoreEntity()
}
