package net.cyberratt.database.entity.converters

import net.cyberratt.web.model.Location
import net.cyberratt.database.entity.LocationEntity
import org.springframework.stereotype.Component

@Component
class LocationEntityConverter : EntityConverter<Location, LocationEntity> {

  override fun toApiModel(entity: LocationEntity) = Location()

  override fun toEntity(apiModel: Location) = LocationEntity()
}
