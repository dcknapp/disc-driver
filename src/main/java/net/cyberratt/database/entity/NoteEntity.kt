package net.cyberratt.database.entity

import javax.persistence.*
import java.util.UUID

@Entity
@Table(name = "notes")
class NoteEntity(

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "note_id")
  override var id: UUID? = null,

  author: UserEntity? = null,
  content: String? = null
) : AuthoredContent(author, content) {

  override fun toString(): String {
    return "NoteEntity(id=$id, content=$content, author=$author)"
  }
}
