package net.cyberratt.database.entity

import java.util.Date
import java.util.UUID
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table
import javax.persistence.Temporal
import javax.persistence.TemporalType

@Entity
@Table(name = "reviews")
class ReviewEntity(

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "review_id")
  override var id: UUID? = null,

  @ManyToOne
  @JoinColumn(name = "course_id")
  var courseEntity: CourseEntity? = null,

  @Column(name = "rating")
  var rating: Int? = null,

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "game_date")
  var gameDate: Date? = null,

  author: UserEntity? = null,
  content: String = ""

) : AuthoredContent(author, content) {

  override fun toString(): String {
    return "ReviewEntity(id=$id, courseEntity=$courseEntity, rating=$rating, gameDate=$gameDate)"
  }
}
