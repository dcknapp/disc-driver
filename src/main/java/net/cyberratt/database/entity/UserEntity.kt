package net.cyberratt.database.entity

import java.util.ArrayList
import java.util.UUID
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.JoinTable
import javax.persistence.ManyToMany
import javax.persistence.ManyToOne
import javax.persistence.Table

@Entity
@Table(name = "users")
class UserEntity(

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "user_id")
  override var id: UUID? = null,

  @Column(name = "first_name")
  var firstName: String? = null,

  @Column(name = "last_name")
  var lastName: String? = null,

  @Column(name = "email")
  var email: String? = null,

  @Column(name = "nickname")
  var nickname: String? = null,

  @ManyToOne
  @JoinColumn(name = "location_id")
  var location: LocationEntity? = null,

  @ManyToMany(fetch = FetchType.LAZY)
  @JoinTable(
    name = "friends",
    joinColumns = [JoinColumn(name = "user_id")],
    inverseJoinColumns = [JoinColumn(name = "friend_id")]
  )
  var friends: MutableList<UserEntity> = ArrayList(),

  @Column(name = "password_hash")
  var passwordHash: String? = null,

  @Column(name = "enabled")
  var isEnabled: Boolean = false

) : BaseEntity() {

  override fun toString(): String {
    return "UserEntity(id=$id, firstName=$firstName, lastName=$lastName, email=$email, nickname=$nickname, location=$location, friends=$friends, passwordHash=$passwordHash, isEnabled=$isEnabled)"
  }
}
