package net.cyberratt.database.entity

import java.util.ArrayList
import java.util.UUID
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.JoinTable
import javax.persistence.ManyToMany
import javax.persistence.ManyToOne
import javax.persistence.OneToMany
import javax.persistence.Table

@Entity
@Table(name = "courses")
class CourseEntity(

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "course_id")
  override var id: UUID? = null,

  @Column(name = "name")
  var name: String? = null,

  @ManyToOne
  @JoinColumn(name = "location_id")
  var location: LocationEntity? = null,

  @Column(name = "description")
  var description: String? = null,

  @Column(name = "number_of_holes")
  var numberOfHoles: Int? = null,

  @OneToMany(mappedBy = "courseEntity")
  var reviews: List<ReviewEntity> = ArrayList(),

  @ManyToMany
  @JoinTable(
    name = "courses_notes",
    joinColumns = [JoinColumn(name = "course_id")],
    inverseJoinColumns = [JoinColumn(name = "note_id")]
  )
  var notes: List<NoteEntity> = ArrayList()

): BaseEntity() {
  override fun toString(): String {
    return "CourseEntity(id=$id, name=$name, location=$location, description=$description, numberOfHoles=$numberOfHoles, reviews=$reviews, notes=$notes)"
  }
}
