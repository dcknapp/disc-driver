package net.cyberratt.database.entity

import java.io.Serializable
import java.util.Date
import java.util.UUID
import javax.persistence.Column
import javax.persistence.MappedSuperclass
import javax.persistence.PreUpdate
import javax.persistence.Temporal
import javax.persistence.TemporalType

@MappedSuperclass
abstract class BaseEntity : Serializable {

  @Column(name = "created_at")
  @Temporal(TemporalType.TIMESTAMP)
  var created: Date = Date()

  @Column(name = "last_modified")
  @Temporal(TemporalType.TIMESTAMP)
  var modified: Date = Date()

  abstract var id: UUID?

  @PreUpdate
  fun updateModifiedTime() {
    modified = Date()
  }

}
