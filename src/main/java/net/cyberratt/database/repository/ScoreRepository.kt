package net.cyberratt.database.repository

import net.cyberratt.database.entity.ScoreEntity
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository
import java.util.UUID

@Repository
interface ScoreRepository : PagingAndSortingRepository<ScoreEntity, UUID>
//    Collection<ScoreEntity> findAllByUser(UserEntity userEntity);
