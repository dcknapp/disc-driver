package net.cyberratt.database.repository

import net.cyberratt.database.entity.LocationEntity
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository

import java.util.UUID

@Repository
interface LocationRepository : PagingAndSortingRepository<LocationEntity, UUID> {

  fun findByLatitudeAndLongitude(latitude: Double, longitude: Double): LocationEntity?

  fun findByStreetAddressAndCityAndStateAndZipCode(
    streetAddress: String,
    city: String,
    state: String,
    zipCode: Int?
  ): LocationEntity?


  fun findByStreetAddressAndZipCode(streetAddress: String, zipCode: Int?): LocationEntity?

  fun findByStreetAddressAndCityAndCountry(
    streetAddress: String,
    city: String,
    country: String
  ): LocationEntity?
}
