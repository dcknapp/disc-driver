package net.cyberratt.database.repository

import net.cyberratt.database.entity.CourseEntity
import net.cyberratt.database.entity.ReviewEntity
import net.cyberratt.database.entity.UserEntity
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository
import java.util.UUID

@Repository
interface ReviewRepository : PagingAndSortingRepository<ReviewEntity, UUID> {

  fun findAllByAuthor(author: UserEntity): Collection<ReviewEntity>

  fun findByAuthorAndCourseEntity(author: UserEntity, courseEntity: CourseEntity): ReviewEntity?

  fun findAllByCourseEntity(courseEntity: CourseEntity): Collection<ReviewEntity>?
}
