package net.cyberratt.database.repository

import net.cyberratt.database.entity.CourseEntity
import net.cyberratt.database.entity.LocationEntity
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository
import java.util.UUID

@Repository
interface CourseRepository : PagingAndSortingRepository<CourseEntity, UUID> {

  fun findByNameAndLocation(name: String, locationEntity: LocationEntity): CourseEntity?

  fun findAllByLocation(locationEntity: LocationEntity): Collection<CourseEntity>?
}
