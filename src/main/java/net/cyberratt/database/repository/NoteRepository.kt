package net.cyberratt.database.repository

import net.cyberratt.database.entity.NoteEntity
import net.cyberratt.database.entity.UserEntity
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository
import java.util.UUID

@Repository
interface NoteRepository : PagingAndSortingRepository<NoteEntity, UUID> {

  fun findAllByAuthor(author: UserEntity): Collection<NoteEntity>
}
