package net.cyberratt.database.repository

import net.cyberratt.database.entity.GameEntity
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository

import java.util.UUID

@Repository
interface GameRepository : PagingAndSortingRepository<GameEntity, UUID>
//    @Query(nativeQuery= true,
//            value = "select g.game_id, g.gameStart, g.gameEnd, g.created_at, g.last_modified from games g inner join games_scores gs on g.game_id = gs.game_id " +
//                    "inner join scores s on s.score_id = gs.score_id inner join users u on u.user_id = s.user_id " +
//                    "where u.first_name = 'Vincent'")
//    List<GameEntity> findByUser(UserEntity userEntity);

//    Collection<GameEntity> findAllByCourseAndUser(CourseEntity courseEntity, UserEntity userEntity);
