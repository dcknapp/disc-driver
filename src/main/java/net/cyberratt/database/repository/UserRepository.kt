package net.cyberratt.database.repository

import net.cyberratt.database.entity.UserEntity
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository
import java.util.UUID

@Repository
interface UserRepository : PagingAndSortingRepository<UserEntity, UUID> {

  fun findByEmail(email: String): UserEntity?

  fun findAllByFirstNameAndLastName(firstName: String, lastName: String): List<UserEntity>

  fun findAllByNickname(nickname: String): List<UserEntity>
}
