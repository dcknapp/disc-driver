package net.cyberratt.exception

open class EntityProviderException(exceptionMessage: ExceptionMessage) : Exception(exceptionMessage.message)
