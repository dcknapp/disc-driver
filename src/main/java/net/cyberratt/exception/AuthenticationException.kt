package net.cyberratt.exception

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(code = HttpStatus.UNAUTHORIZED)
class AuthenticationException : EntityProviderException(ExceptionMessage.AUTHENTICATION_FAILURE)
