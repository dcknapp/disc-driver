package net.cyberratt.exception

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Unexpected argument mismatch")
class ArgumentMismatchException : EntityProviderException(ExceptionMessage.ENTITY_URI_ID_MISMATCH)
