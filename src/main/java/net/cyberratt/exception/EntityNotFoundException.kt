package net.cyberratt.exception

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Entity was not found")
class EntityNotFoundException : EntityProviderException(ExceptionMessage.ENTITY_NOT_FOUND)
