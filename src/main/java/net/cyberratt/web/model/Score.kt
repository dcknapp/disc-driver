package net.cyberratt.web.model

import java.util.Date

data class Score(
  override var id: String? = null,
  var user: User? = null,
  var holeScores: Map<Int, Int> = HashMap(),
  override var created: Date? = null,
  override var modified: Date? = null
) : ApiModel
