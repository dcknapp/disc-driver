package net.cyberratt.web.model

import org.hibernate.validator.constraints.NotEmpty
import java.util.Date
import javax.validation.constraints.NotNull

data class Game(
  override var id: String? = null,
  var course: Course? = null,
  var gameStart: Date? = null,
  var gameEnd: Date? = null,

  @NotEmpty
  @NotNull
  var scores: Collection<Score>? = null,
  override var created: Date? = null,
  override var modified: Date? = null
) : ApiModel
