package net.cyberratt.web.model

import org.hibernate.validator.constraints.NotEmpty
import java.util.ArrayList
import java.util.Date

data class Course(
  override var id: String? = null,
  @NotEmpty var name: String = "",
  var location: Location? = null,
  var description: String = "",
  var numberOfHoles: Int = 18,
  var reviews: Collection<Review> = ArrayList(),
  var notes: Collection<Note> = ArrayList(),
  override var created: Date? = null,
  override var modified: Date? = null
) : ApiModel
