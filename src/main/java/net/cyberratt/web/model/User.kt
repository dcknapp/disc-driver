package net.cyberratt.web.model

import com.fasterxml.jackson.annotation.JsonIgnore
import java.util.ArrayList
import java.util.Date

data class User(
  override var id: String? = null,
  var firstName: String? = null,
  var lastName: String? = null,
  var email: String? = null,
  var nickname: String? = null,
  var location: Location? = null,
  var friends: List<User> = ArrayList(),
  var password: String? = null,
  @JsonIgnore
  var passwordHash: String? = null,
  var isEnabled: Boolean = false,
  override var created: Date? = null,
  override var modified: Date? = null
) : ApiModel
