package net.cyberratt.web.model

import java.util.Date

data class Note(
  override var id: String? = null,
  var author: User? = null,
  var content: String? = null,
  override var created: Date? = null,
  override var modified: Date? = null
) : ApiModel
