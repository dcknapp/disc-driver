package net.cyberratt.web.model

import java.util.Date

data class Review(
  override var id: String? = null,
  var course: Course? = null,
  var rating: Int? = null,
  var gameDate: Date? = null,
  var author: User? = null,
  var content: String? = null,
  override var created: Date? = null,
  override var modified: Date? = null
) : ApiModel
