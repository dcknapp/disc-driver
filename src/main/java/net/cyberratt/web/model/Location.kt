package net.cyberratt.web.model

import java.util.Date

data class Location(
  override var id: String? = null,
  var latitude: Double = 0.toDouble(),
  var longitude: Double = 0.toDouble(),
  var streetAddress: String? = null,
  var city: String? = null,
  var state: String? = null,
  var country: String? = null,
  var zipCode: Int? = null,
  override var created: Date? = null,
  override var modified: Date? = null
) : ApiModel
