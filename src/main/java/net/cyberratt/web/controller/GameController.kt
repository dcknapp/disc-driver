package net.cyberratt.web.controller

import net.cyberratt.web.model.Game
import net.cyberratt.service.EntityService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/games")
class GameController : BaseController<Game>() {

  @Autowired
  override lateinit var entityService: EntityService<Game>

  @GetMapping
  override fun getAllEntities(): ResponseEntity<List<Game>> {
    return super.getAllEntities()
  }

  @GetMapping("/{id}")
  override fun getEntity(@PathVariable("id") id: String): ResponseEntity<Game> {
    return super.getEntity(id)
  }

  @PostMapping
  override fun createEntity(@RequestBody entity: Game): ResponseEntity<Game> {
    return super.createEntity(entity)
  }

  @PutMapping
  override fun updateEntity(@RequestBody entity: Game): ResponseEntity<Void> {
    return super.updateEntity(entity)
  }

  @DeleteMapping("/{id}")
  override fun deleteEntity(@PathVariable("id") id: String): ResponseEntity<Void> {
    return super.deleteEntity(id)
  }

  public override fun getLogger(): Logger = logger

  companion object {
    private val logger = LoggerFactory.getLogger(GameController::class.java)
  }
}
