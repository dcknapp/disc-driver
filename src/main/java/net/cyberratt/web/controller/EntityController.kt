package net.cyberratt.web.controller

import net.cyberratt.exception.EntityProviderException
import org.springframework.http.ResponseEntity

interface EntityController<T> {
  /**
   * Retrieve all entities of a type
   * @return
   */
  fun getAllEntities(): ResponseEntity<List<T>>

  /**
   * Persists a new entity if no entity already exists with unique-enforced information
   * @param entity
   * @return
   */
  fun createEntity(entity: T): ResponseEntity<T>

  /**
   * Returns a single entity referenced by ID
   * @param id
   * @return
   */
  fun getEntity(id: String): ResponseEntity<T>

  /**
   * Update existing entity with submitted request body
   * @param entity
   * @return
   */
  fun updateEntity(entity: T): ResponseEntity<Void>

  /**
   * Sets status of entity to deleted
   * @param id
   * @return
   */
  fun deleteEntity(id: String): ResponseEntity<Void>
}
