package net.cyberratt.web.controller

import net.cyberratt.web.model.User
import net.cyberratt.service.EntityService
import net.cyberratt.service.UserService
import net.cyberratt.web.requests.FriendRequest
import net.cyberratt.web.requests.LoginRequest
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.HttpStatus.OK
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@RestController
@RequestMapping("/users")
class UserController : BaseController<User>() {

  @Autowired
  override lateinit var entityService: EntityService<User>

  @GetMapping
  override fun getAllEntities(): ResponseEntity<List<User>> {
    return super.getAllEntities()
  }

  @GetMapping("/{id}")
  fun getEntity(
    @PathVariable("id") id: String,
    @RequestParam("friends", required = false) friends: Boolean = false
  ): ResponseEntity<User> {
    logger.info("Will get friends when this works? $friends")
    return super.getEntity(id)
  }

  @PostMapping
  override fun createEntity(@RequestBody entity: User): ResponseEntity<User> {
    return super.createEntity(entity)
  }

  @PutMapping
  override fun updateEntity(@RequestBody entity: User): ResponseEntity<Void> {
    return super.updateEntity(entity)
  }

  @DeleteMapping("/{id}")
  override fun deleteEntity(@PathVariable("id") id: String): ResponseEntity<Void> {
    return super.deleteEntity(id)
  }

  @PostMapping("/login")
  fun login(@Valid @RequestBody loginRequest: LoginRequest): ResponseEntity<User> {
    val user = (entityService as UserService).login(loginRequest)
    return ResponseEntity(user, HttpStatus.OK)
  }

  @PostMapping("/friends")
  fun addFriend(@Valid @RequestBody request: FriendRequest): ResponseEntity<Void> {
    return if ((entityService as UserService).addFriend(request)) {
      ResponseEntity(HttpStatus.CREATED)
    } else {
      ResponseEntity(HttpStatus.NOT_MODIFIED)
    }
  }

  @GetMapping("/{id}/friends")
  fun getFriends(@PathVariable("id") id: String): ResponseEntity<List<User>> {
    val friends = (entityService as UserService).getFriends(id)
    return ResponseEntity(friends, OK)
  }

  public override fun getLogger(): Logger = logger

  companion object {
    private val logger = LoggerFactory.getLogger(UserController::class.java)
  }
}
