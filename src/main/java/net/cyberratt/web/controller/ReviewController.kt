package net.cyberratt.web.controller

import net.cyberratt.web.model.Review
import net.cyberratt.service.EntityService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/reviews")
class ReviewController : BaseController<Review>() {

  @Autowired
  override lateinit var entityService: EntityService<Review>

  @GetMapping
  override fun getAllEntities(): ResponseEntity<List<Review>> {
    return super.getAllEntities()
  }

  @GetMapping("/{id}")
  override fun getEntity(@PathVariable("id") id: String): ResponseEntity<Review> {
    return super.getEntity(id)
  }

  @PostMapping
  override fun createEntity(@RequestBody entity: Review): ResponseEntity<Review> {
    return super.createEntity(entity)
  }

  @PutMapping
  override fun updateEntity(@RequestBody entity: Review): ResponseEntity<Void> {
    return super.updateEntity(entity)
  }

  @DeleteMapping("/{id}")
  override fun deleteEntity(@PathVariable("id") id: String): ResponseEntity<Void> {
    return super.deleteEntity(id)
  }

  public override fun getLogger(): Logger = logger

  companion object {
    private val logger = LoggerFactory.getLogger(ReviewController::class.java)
  }
}
