package net.cyberratt.web.controller

import net.cyberratt.exception.EntityProviderException
import net.cyberratt.exception.ExceptionMessage
import net.cyberratt.service.EntityService
import org.slf4j.Logger
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity

abstract class BaseController<T> : EntityController<T> {

  // TODO implement fields request parameter utilization
  // TODO implement pagination

  protected abstract val entityService: EntityService<T>

  override fun getAllEntities(): ResponseEntity<List<T>> {
      getLogger().info("Retrieving all the things")

      val users = entityService.getAllEntities()

      return ResponseEntity(users, HttpStatus.OK)
    }

  override fun getEntity(id: String): ResponseEntity<T> {

    getLogger().info("Retrieving thing: {}", id)

    val result = entityService.getEntityById(id)

    return ResponseEntity(result, HttpStatus.OK)
  }

  override fun createEntity(entity: T): ResponseEntity<T> {

    getLogger().info("Creating thing: {}", entity)

    val createdEntity =
      entityService.createEntity(entity) ?: throw EntityProviderException(ExceptionMessage.UNKNOWN_ERROR)

    return ResponseEntity(createdEntity, HttpStatus.CREATED)
  }

  override fun updateEntity(entity: T): ResponseEntity<Void> {

    getLogger().info("Updating thing {}", entity)

    val result = entityService.updateEntity(entity)

    getLogger().info("Thing updated to: {}", result)

    return ResponseEntity(HttpStatus.NO_CONTENT)
  }

  override fun deleteEntity(id: String): ResponseEntity<Void> {
    getLogger().info("Deleting entity {}", id)

    var returnStatus = HttpStatus.NO_CONTENT
    val isDeleted = entityService.deleteEntity(id)

    if (!isDeleted) {
      getLogger().warn("Entity not deleted")
      returnStatus = HttpStatus.NOT_MODIFIED
    }

    getLogger().info("Entity deleted")

    return ResponseEntity(returnStatus)
  }

  protected abstract fun getLogger(): Logger
}
