package net.cyberratt.web.requests

import org.hibernate.validator.constraints.NotBlank

data class FriendRequest(
  @NotBlank var userId: String = "",
  @NotBlank var friendId: String = ""
)
