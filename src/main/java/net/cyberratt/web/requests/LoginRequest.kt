package net.cyberratt.web.requests

import org.hibernate.validator.constraints.NotBlank

data class LoginRequest(
  @NotBlank var email: String = "",
  @NotBlank var password: CharSequence = ""
)
