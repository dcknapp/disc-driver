package net.cyberratt.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import java.security.NoSuchAlgorithmException
import java.security.SecureRandom

@Configuration
@EnableWebSecurity
open class SecurityConfiguration : WebSecurityConfigurerAdapter() {

  override fun configure(http: HttpSecurity) {
    http.csrf().disable()
  }

  @Bean open fun encoder(): BCryptPasswordEncoder {
    val random: SecureRandom
    try {
      random = SecureRandom.getInstance("NativePRNG")
    } catch (ex: NoSuchAlgorithmException) {
      ex.printStackTrace()
      throw IllegalStateException("Unable to setup security settings")
    }

    return BCryptPasswordEncoder(15, random)
  }
}
