package net.cyberratt.config

import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.context.annotation.PropertySource

@Configuration
@Import(
  DatabaseConfiguration::class,
  RepositoryConfiguration::class,
  SecurityConfiguration::class,
  ServiceConfiguration::class
)
@ComponentScan("net.cyberratt")
@PropertySource("classpath:application.properties")
open class AppConfig
