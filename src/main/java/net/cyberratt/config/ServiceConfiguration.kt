package net.cyberratt.config

import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.annotation.Configuration

@Configuration
@EntityScan("net.cyberratt.service")
open class ServiceConfiguration {

//  @Bean open fun courseService(repository: CourseRepository) = CourseService(repository)
//
//  @Bean open fun gameService(repository: GameRepository) = GameService(repository)
//
//  @Bean open fun locationService(repository: LocationRepository) = LocationService(repository)
//
//  @Bean open fun noteService(repository: NoteRepository) = NoteService(repository)
//
//  @Bean open fun reviewService(repository: ReviewRepository) = ReviewService(repository)
//
//  @Bean open fun scoreService(repository: ScoreRepository) = ScoreService(repository)
//
//  @Bean open fun userService(
//    repository: UserRepository,
//    encoder: BCryptPasswordEncoder
//  ): EntityService<User> {
//
//    return UserService(repository, encoder)
//  }
}
