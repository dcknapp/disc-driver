package net.cyberratt.config

import net.cyberratt.database.entity.CourseEntity
import net.cyberratt.database.entity.LocationEntity
import net.cyberratt.database.entity.ReviewEntity
import net.cyberratt.database.entity.UserEntity
import net.cyberratt.database.repository.CourseRepository
import net.cyberratt.database.repository.LocationRepository
import net.cyberratt.database.repository.ReviewRepository
import net.cyberratt.database.repository.UserRepository
import org.joda.time.DateTime
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.InitializingBean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service

@Service
class DataBootstrapper : InitializingBean {

  @Autowired private lateinit var userRepository: UserRepository
  @Autowired private lateinit var courseRepository: CourseRepository
  @Autowired private lateinit var locationRepository: LocationRepository
  @Autowired private lateinit var reviewRepository: ReviewRepository
  @Autowired private lateinit var encoder: BCryptPasswordEncoder
  
  override fun afterPropertiesSet() {
    logger.info("Bootstrapping test data")
    createUser()
    createNorthShoresCourse()
    createReview()
  }

  private fun createUser(): UserEntity {
    logger.info("... saving userEntity")
    var userEntity: UserEntity? = userRepository.findByEmail("va@asdf.com")
    if (userEntity == null) {
      userEntity = UserEntity()

      userEntity.firstName = "Vincent"
      userEntity.lastName = "Adultman"
      userEntity.location = createFakeLosAngelesLocation()
      userEntity.email = "va@asdf.com"
      userEntity.passwordHash = encoder.encode("asdfasdf")
      userEntity.isEnabled = true
      userEntity.nickname = "definitely_an_adult"

      userRepository.save(userEntity)
    }
    return userEntity
  }

  private fun createFakeLosAngelesLocation(): LocationEntity {
    logger.info("... saving fake LA location")
    val streetAddress = "1234 Some St."
    val city = "Los Angeles"
    val country = "USA"
    var loc: LocationEntity? = locationRepository.findByStreetAddressAndCityAndCountry(streetAddress, city, country)
    if (loc == null) {
      loc = LocationEntity()
      loc.city = city
      loc.state = "CA"
      loc.zipCode = 90210
      loc.country = country
      loc.streetAddress = streetAddress
      loc.latitude = 34.052234
      loc.longitude = -118.243685

      return locationRepository.save(loc)
    }
    return loc

  }

  private fun createNorthShoresCourse(): CourseEntity {
    logger.info("Saving courseEntity")
    val loc = createLakeFayettevilleLocation()
    val name = "North Shores Disc Golf CourseEntity"
    var courseEntity: CourseEntity? = courseRepository.findByNameAndLocation(name, loc)
    if (courseEntity == null) {
      courseEntity = CourseEntity()
      courseEntity.description = "18-hole courseEntity at Lake Fayetteville"
      courseEntity.name = name
      courseEntity.numberOfHoles = 18
      courseEntity.location = loc

      return courseRepository.save(courseEntity)
    }
    return courseEntity
  }

  private fun createLakeFayettevilleLocation(): LocationEntity {
    val streetAddress = "1111 street street"
    val city = "Fayetteville"
    val country = "USA"
    var loc: LocationEntity? = locationRepository.findByStreetAddressAndCityAndCountry(streetAddress, city, country)
    if (loc == null) {
      loc = LocationEntity()
      loc.streetAddress = streetAddress
      loc.country = country
      loc.city = city
      loc.state = "AR"
      loc.zipCode = 72703

      return locationRepository.save(loc)
    }

    return loc
  }

  private fun createReview(): ReviewEntity {
    val author = userRepository.findByEmail("va@asdf.com") ?: createUser()
    val course = createNorthShoresCourse()
    var reviewEntity: ReviewEntity? = reviewRepository.findByAuthorAndCourseEntity(author, course)
    if (reviewEntity == null) {
      reviewEntity = ReviewEntity(author = author)
      reviewEntity.author = author
      reviewEntity.content = "This courseEntity is great. The lake is beautiful and it has a lot of good long holes"
      reviewEntity.gameDate = DateTime.now().minusDays(5).minusYears(1).minusHours(2).toDate() // whatever. random date
      reviewEntity.courseEntity = course

      return reviewRepository.save(reviewEntity)
    }
    return reviewEntity
  }

  companion object {

    private val logger = LoggerFactory.getLogger(DataBootstrapper::class.java)
  }
}
