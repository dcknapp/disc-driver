package net.cyberratt.helper

import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

import java.util.HashMap

import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertNotNull

@RunWith(JUnit4::class)
class ScoreHelperTest {

  @Test
  fun shouldConvertScoreMapToDelimitedString() {
    val numHoles = 18
    val scoreMap = buildScoreMap(numHoles)
    val result = ScoreHelper.toDelimitedString(scoreMap)
    val expected = buildScoreString(numHoles)

    assertNotNull(result)
    assertFalse(result.isEmpty())
    assertEquals(expected, result)
  }

  @Test
  fun shouldConvertScoreStringToScoreMap() {
    val numHoles = 18

    val scoreString = buildScoreString(numHoles)
    val result = ScoreHelper.toHoleNumberedMap(scoreString)
    val expected = buildScoreMap(numHoles)

    assertNotNull(result)
    assertFalse(result.isEmpty())
    assertEquals(expected, result)
  }

  @Test
  fun shouldConvertToScoreList() {
    val numHoles = 18

    val scoreString = buildScoreString(numHoles)
    val scoreList = ScoreHelper.toScoreList(scoreString)

    assertNotNull(scoreList)
    assertFalse(scoreList.isEmpty())
    assertEquals(numHoles.toLong(), scoreList.size.toLong())
    for (score in scoreList) {
      assertEquals(Integer.valueOf(3), score)
    }
  }

  @Test
  fun shouldDivideGameHolesProperlyWithEvenNumberOfHoles() {
    val firstFrontHoleNumber = ScoreHelper.getFirstBackHalfHoleNumber(18)

    assertNotNull(firstFrontHoleNumber)
    assertEquals(Integer.valueOf(9), firstFrontHoleNumber)
  }

  @Test
  fun shouldDivideGameHolesProperlyWithOddNumberOfHoles() {
    val firstFrontHoleNumber = ScoreHelper.getFirstBackHalfHoleNumber(19)

    assertNotNull(firstFrontHoleNumber)
    assertEquals(Integer.valueOf(10), firstFrontHoleNumber)
  }

  @Test
  fun shouldGetTotalScore() {
    val numHoles = 18
    val scoreString = buildScoreString(numHoles)
    val totalScore = ScoreHelper.getTotalScore(scoreString)

    assertNotNull(totalScore)
    assertEquals(Integer.valueOf(18 * 3), totalScore)
  }

  @Test
  fun shouldGetCorrectScoresForEvenNumberedSetOfHoles() {
    val numHoles = 18
    val scoreString = buildScoreString(numHoles)
    val frontScore = ScoreHelper.getFrontScore(scoreString)
    val backScore = ScoreHelper.getBackScore(scoreString)

    assertNotNull(frontScore)
    assertNotNull(backScore)
    assertEquals(Integer.valueOf(numHoles / 2 * 3), frontScore)
    assertEquals(Integer.valueOf(numHoles / 2 * 3), backScore)
  }

  @Test
  fun shouldGetCorrectScoresForOddNumberedSetOfHoles() {
    val numHoles = 19
    val scoreString = buildScoreString(numHoles)
    val frontScore = ScoreHelper.getFrontScore(scoreString)
    val backScore = ScoreHelper.getBackScore(scoreString)
    assertNotNull(frontScore)
    assertNotNull(backScore)
    assertEquals(Integer.valueOf((numHoles / 2 + 1) * 3), frontScore)
    assertEquals(Integer.valueOf(numHoles / 2 * 3), backScore)
  }

  private fun buildScoreMap(numHoles: Int): Map<Int, Int> {
    val scoreMap = HashMap<Int, Int>()

    for (i in 1..numHoles) {
      scoreMap[i] = 3
    }
    return scoreMap
  }

  private fun buildScoreString(numHoles: Int): String {
    var scoreString = ""
    for (i in 1..numHoles) {
      scoreString += "3"
      if (i < numHoles)
        scoreString += "|"
    }
    return scoreString
  }
}
