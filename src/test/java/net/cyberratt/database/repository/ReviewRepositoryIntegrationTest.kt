package net.cyberratt.database.repository

import net.cyberratt.config.TestAppConfig
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertNotNull
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.support.AnnotationConfigContextLoader
import javax.transaction.Transactional

@RunWith(SpringJUnit4ClassRunner::class)
@ContextConfiguration(loader = AnnotationConfigContextLoader::class, classes = [(TestAppConfig::class)])
@Transactional
open class ReviewRepositoryIntegrationTest {

  @Autowired
  private lateinit var courseRepository: CourseRepository
  @Autowired
  private lateinit var locationRepository: LocationRepository
  @Autowired
  private lateinit var reviewRepository: ReviewRepository
  @Autowired
  private lateinit var userRepository: UserRepository

  @Test
  fun repositoryIsNotNull() {
    assertNotNull(reviewRepository)
  }

  @Test
  fun shouldFindAllByAuthor() {
    val userEntity = userRepository.findByEmail("va@asdf.com")
    val results = reviewRepository.findAllByAuthor(userEntity!!)
    assertNotNull(results)
    assertFalse(results.isEmpty())
    for (reviewEntity in results) {
      assertEquals(reviewEntity.author, userEntity)
    }
  }

  @Test
  fun shouldFindByAuthorAndCourse() {
    val streetAddress = "1111 street street"
    val city = "Fayetteville"
    val country = "USA"
    val loc = locationRepository.findByStreetAddressAndCityAndCountry(streetAddress, city, country)
    val name = "North Shores Disc Golf CourseEntity"
    val courseEntity = courseRepository.findByNameAndLocation(name, loc!!)
    val userEntity = userRepository.findByEmail("va@asdf.com")
    val result = reviewRepository.findByAuthorAndCourseEntity(userEntity!!, courseEntity!!)
    assertNotNull(result)
    assertEquals(result?.courseEntity, courseEntity)
    assertEquals(result?.author, userEntity)
  }

  @Test
  fun shouldFindAllByCourse() {
    val streetAddress = "1111 street street"
    val city = "Fayetteville"
    val country = "USA"
    val loc = locationRepository.findByStreetAddressAndCityAndCountry(streetAddress, city, country)
    val name = "North Shores Disc Golf CourseEntity"
    val course = courseRepository.findByNameAndLocation(name, loc!!)
    val results = reviewRepository.findAllByCourseEntity(course!!)
    assertNotNull(results)
    assertFalse(results!!.isEmpty())
    for (reviewEntity in results) {
      assertEquals(reviewEntity.courseEntity, course)
    }
  }
}
