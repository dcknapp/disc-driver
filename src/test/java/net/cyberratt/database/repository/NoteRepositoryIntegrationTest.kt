package net.cyberratt.database.repository

import net.cyberratt.config.TestAppConfig
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertNotNull
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.support.AnnotationConfigContextLoader
import javax.transaction.Transactional

@RunWith(SpringJUnit4ClassRunner::class)
@ContextConfiguration(loader = AnnotationConfigContextLoader::class, classes = [(TestAppConfig::class)])
@Transactional
open class NoteRepositoryIntegrationTest {

  @Autowired
  private lateinit var noteRepository: NoteRepository
  @Autowired
  private lateinit var userRepository: UserRepository

  @Test
  fun repositoryIsNotNull() {
    assertNotNull(noteRepository)
  }

  @Test
  fun shouldFindAllByAuthor() {
    val userEntity = userRepository.findByEmail("va@asdf.com")
    val notes = noteRepository.findAllByAuthor(userEntity!!)
    assertNotNull(notes)
    assertFalse(notes.isEmpty())
    for (noteEntity in notes) {
      assertEquals(noteEntity.author, userEntity)
    }
  }
}
