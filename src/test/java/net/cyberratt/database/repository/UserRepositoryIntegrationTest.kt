package net.cyberratt.database.repository

import junit.framework.TestCase.assertFalse
import net.cyberratt.config.TestAppConfig
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.support.AnnotationConfigContextLoader
import javax.transaction.Transactional

@RunWith(SpringJUnit4ClassRunner::class)
@ContextConfiguration(loader = AnnotationConfigContextLoader::class, classes = [(TestAppConfig::class)])
@Transactional
open class UserRepositoryIntegrationTest {

  @Autowired
  private lateinit var userRepository: UserRepository

  @Test
  fun repositoryIsNotNull() {
    assertNotNull(userRepository)
  }

  @Test
  fun shouldFindByFirstAndLastName() {
    val firstName = "Vincent"
    val lastName = "Adultman"
    val results = userRepository.findAllByFirstNameAndLastName(firstName, lastName)
    assertNotNull(results)
    assertFalse(results.isEmpty())
    for (userEntity in results) {
      assertEquals(userEntity.firstName, firstName)
      assertEquals(userEntity.lastName, lastName)
    }
  }

  @Test
  fun shouldFindByNickName() {
    val nickname = "definitely_an_adult"
    val results = userRepository.findAllByNickname(nickname)
    assertNotNull(results)
    assertFalse(results.isEmpty())
    for (userEntity in results) {
      assertEquals(userEntity.nickname, nickname)
    }
  }
}
