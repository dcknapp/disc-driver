package net.cyberratt.database.repository

import net.cyberratt.config.TestAppConfig
import net.cyberratt.database.entity.LocationEntity
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.`when`
import org.mockito.Mockito.any
import org.mockito.Mockito.mock
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.support.AnnotationConfigContextLoader
import java.util.UUID
import javax.transaction.Transactional

@RunWith(SpringJUnit4ClassRunner::class)
@ContextConfiguration(loader = AnnotationConfigContextLoader::class, classes = [(TestAppConfig::class)])
@Transactional
open class LocationRepositoryIntegrationTest {

  @Autowired
  private lateinit var locationRepository: LocationRepository

  @Test
  fun repositoryIsNotNull() {
    assertNotNull(locationRepository)
  }

  @Test
  fun shouldMockInSpringTestRunner() {
    val mockRepo = mock(LocationRepository::class.java)
    val mockLocation = LocationEntity()
    mockLocation.city = "Fake city"
    mockLocation.country = "fake country"
    mockLocation.latitude = 0.0
    mockLocation.longitude = 0.0
    mockLocation.zipCode = 12345

    `when`(mockRepo.findOne(any(UUID::class.java))).thenReturn(mockLocation)

    val result = mockRepo.findOne(UUID.randomUUID())

    assertEquals(result, mockLocation)
  }

  @Test
  fun shouldFindByLatitudeAndLongitude() {
    val latitude = 34.052234
    val longitude = -118.243685
    val result = locationRepository.findByLatitudeAndLongitude(latitude, longitude)
    assertNotNull(result)
    assertEquals(result!!.latitude, latitude, .001)
    assertEquals(result.longitude, longitude, .001)
  }

  @Test
  fun shouldFindByStreetAddressAndZipCode() {
    val streetAddress = "1234 Some St."
    val zipCode = 90210
    val result = locationRepository.findByStreetAddressAndZipCode(streetAddress, zipCode)
    assertNotNull(result)
    assertEquals(result!!.zipCode, zipCode)
    assertEquals(result.streetAddress, streetAddress)
  }

  @Test
  fun shouldFindByStreetAddressCityStateAndZipCode() {
    val streetAddress = "1234 Some St."
    val city = "Los Angeles"
    val state = "CA"
    val zipCode = 90210
    val result = locationRepository.findByStreetAddressAndCityAndStateAndZipCode(
      streetAddress, city, state, zipCode
    )
    assertNotNull(result)
    assertEquals(result!!.zipCode, zipCode)
    assertEquals(result.streetAddress, streetAddress)
    assertEquals(result.city, city)
    assertEquals(result.state, state)
  }

  @Test
  fun shouldFindByStreetAddressCityAndZipCode() {
    val streetAddress = "1234 Some St."
    val city = "Los Angeles"
    val country = "USA"
    val result = locationRepository.findByStreetAddressAndCityAndCountry(streetAddress, city, country)
    assertNotNull(result)
    assertEquals(result!!.streetAddress, streetAddress)
    assertEquals(result.city, city)
    assertEquals(result.country, country)
  }
}
