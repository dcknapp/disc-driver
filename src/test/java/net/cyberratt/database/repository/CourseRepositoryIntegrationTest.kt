package net.cyberratt.database.repository

import net.cyberratt.config.TestAppConfig
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.support.AnnotationConfigContextLoader
import javax.transaction.Transactional

@RunWith(SpringJUnit4ClassRunner::class)
@ContextConfiguration(loader = AnnotationConfigContextLoader::class, classes = [(TestAppConfig::class)])
@Transactional
open class CourseRepositoryIntegrationTest {

  @Autowired
  private lateinit var courseRepository: CourseRepository
  @Autowired
  private lateinit var locationRepository: LocationRepository

  @Test
  fun repositoryIsNotNull() {
    assertNotNull(locationRepository)
    assertNotNull(courseRepository)
  }

  @Test
  fun shouldFindCourseByNameAndLocation() {
    val courseName = "North Shores Disc Golf Course"
    val streetAddress = "1111 street street"
    val city = "Fayetteville"
    val country = "USA"
    val location = locationRepository.findByStreetAddressAndCityAndCountry(streetAddress, city, country)
    assertNotNull(location)
    val result = courseRepository.findByNameAndLocation(courseName, location!!)
    assertNotNull(result)
    assertEquals(result!!.name, courseName)
    assertNotNull(result.location)
    assertEquals(result.location?.id, location.id)
  }
}
