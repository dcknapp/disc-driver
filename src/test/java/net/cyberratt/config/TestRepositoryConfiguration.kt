package net.cyberratt.config

import net.cyberratt.database.repository.*
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaRepositories

import org.mockito.Mockito.mock

@Configuration
open class TestRepositoryConfiguration {

  @Bean
  open fun courseRepository(): CourseRepository {
    return mock(CourseRepository::class.java)
  }

  @Bean
  open fun gameRepository(): GameRepository {
    return mock(GameRepository::class.java)
  }

  @Bean
  open fun locationRepository(): LocationRepository {
    return mock(LocationRepository::class.java)
  }

  @Bean
  open fun noteRepository(): NoteRepository {
    return mock(NoteRepository::class.java)
  }

  @Bean
  open fun reviewRepository(): ReviewRepository {
    return mock(ReviewRepository::class.java)
  }

  @Bean
  open fun scoreRepository(): ScoreRepository {
    return mock(ScoreRepository::class.java)
  }

  @Bean
  open fun userRepository(): UserRepository {
    return mock(UserRepository::class.java)
  }
}
