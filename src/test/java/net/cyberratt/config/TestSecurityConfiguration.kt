package net.cyberratt.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder

import org.mockito.Mockito.mock

@Configuration
@EnableWebSecurity
open class TestSecurityConfiguration {

  @Bean()
  internal open fun mockEncoder(): BCryptPasswordEncoder {
    return mock(BCryptPasswordEncoder::class.java)
  }
}
