package net.cyberratt.config

import net.cyberratt.web.model.*
import net.cyberratt.service.*
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

import org.mockito.Mockito.mock

@Configuration
open class TestServiceConfiguration {

  @Bean
  open fun courseService(): EntityService<Course> {
    return mock(CourseService::class.java)
  }

  @Bean
  open fun gameService(): EntityService<Game> {
    return mock(GameService::class.java)
  }

  @Bean
  open fun locationService(): EntityService<Location> {
    return mock(LocationService::class.java)
  }

  @Bean
  open fun noteService(): EntityService<Note> {
    return mock(NoteService::class.java)
  }

  @Bean
  open fun reviewService(): EntityService<Review> {
    return mock(ReviewService::class.java)
  }

  @Bean
  open fun scoreService(): EntityService<Score> {
    return mock(ScoreService::class.java)
  }

  @Bean
  open fun userService(): EntityService<User> {
    return mock(UserService::class.java)
  }
}
