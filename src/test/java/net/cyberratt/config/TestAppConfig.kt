package net.cyberratt.config

import org.springframework.boot.SpringBootConfiguration
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Import
import org.springframework.context.annotation.PropertySource

@SpringBootConfiguration
@Import(
  TestDatabaseConfiguration::class,
  TestRepositoryConfiguration::class,
  TestSecurityConfiguration::class,
  TestServiceConfiguration::class
)
@ComponentScan("net.cyberratt")
@PropertySource("classpath:application-test.properties")
open class TestAppConfig
