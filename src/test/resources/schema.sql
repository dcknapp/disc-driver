# drop database if exists driver_app_schema;
create database if not exists driver_app_schema;
use driver_app_schema;

create table if not exists locations (
    location_id binary(16) not null,
    name varchar(50),
    latitude decimal(8, 6),
    longitude decimal(9, 6),
    street_address varchar(40),
    city varchar(60),
    state varchar(20),
    country varchar(25),
    zip_code mediumint(5),
    created_at datetime not null default now(),
    last_modified datetime not null default now(),
    primary key (`location_id`)
);

create table if not exists users (
    user_id binary(16) not null,
    location_id binary(16),
    first_name varchar(30) not null,
    last_name varchar(30) not null,
    email varchar(50) not null,
    nickname varchar(30),
    password_hash varchar(60) not null,
    enabled tinyint(1) default 0,
    created_at datetime not null default now(),
    last_modified datetime not null default now(),
    primary key (`user_id`),
    foreign key (`location_id`) references locations (`location_id`) on delete cascade
);

create table if not exists friends (
    user_id binary(16) not null,
    friend_id binary(16) not null,
    primary key (`user_id`, `friend_id`),
    index friend_idx (`friend_id`),
    constraint friend_fk foreign key (`friend_id`) references users (`user_id`) on delete cascade,
    constraint friend_user_fk foreign key (`user_id`) references users (`user_id`) on delete cascade
);

create table if not exists scores (
    score_id binary(16) not null,
    user_id binary(16),
    hole_scores varchar(70),
    created_at datetime not null default now(),
    last_modified datetime not null default now(),
    primary key (`score_id`),
    foreign key (`user_id`) references users (`user_id`) on delete cascade
);

create table if not exists games (
    game_id binary(16) not null,
    course_id binary(16),
    game_start datetime not null,
    game_end datetime not null,
    created_at datetime not null default now(),
    last_modified datetime not null default now(),
    primary key (`game_id`)
);

create table if not exists games_scores (
    game_id binary(16) not null,
    score_id binary(16) not null,
    primary key (`game_id`, `score_id`),
    index score_idx (`score_id`),
    constraint game_fk foreign key (`game_id`) references games (`game_id`) on delete cascade,
    constraint score_fk foreign key (`score_id`) references scores (`score_id`) on delete cascade
);

create table if not exists courses (
    course_id binary(16) not null,
    name varchar(75),
    location_id binary(16),
    description varchar(150),
    number_of_holes tinyint(1),
    created_at datetime not null default now(),
    last_modified datetime not null default now(),
    primary key (`course_id`),
    foreign key (`location_id`) references locations (`location_id`) on delete cascade
);

create table if not exists reviews (
    review_id binary(16) not null,
    author_id binary(16),
    course_id binary(16) not null,
    content varchar(200) not null default '',
    rating tinyint,
    game_date datetime,
    created_at datetime not null default now(),
    last_modified datetime not null default now(),
    primary key (`review_id`),
    foreign key (`author_id`) references users (`user_id`) on delete cascade,
    foreign key (`course_id`) references courses (`course_id`) on delete cascade
);

create table if not exists noteEntities (
    note_id binary(16) not null,
    author_id binary(16) not null,
    content varchar(200) not null default '',
    created_at datetime not null default now(),
    last_modified datetime not null default now(),
    primary key (`note_id`),
    foreign key (`author_id`) references users (`user_id`) on delete cascade
);

create table if not exists courses_notes (
    course_id binary(16) not null,
    note_id binary(16) not null,
    primary key (`course_id`, `note_id`),
    index note_idx (`note_id`),
    constraint note_fk foreign key (`note_id`) references noteEntities (`note_id`) on delete cascade,
    constraint note_course_fk foreign key (`course_id`) references courses (`course_id`) on delete cascade
);
